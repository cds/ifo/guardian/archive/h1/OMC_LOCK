######## H1 OMC Guardian Parameters

# The IFO name
IFO = 'H1'

# threshold for locking on CR_TEM00
lsc_trigger_threshold = 1.0
# the gain of the lsc loop
lsc_gain = 24#this should have been increased to 24 from 3 when the dither amplitude was decreased (LHO alog 30380) but was missed until Jan 2016 resulting in glitches at 1084 Hz.  
# the dither frequency for the lsc loop
lsc_dither_freq = 4190.0 #alog 76587,  used to be 4100 from alog 18130
# the filters to turn on in the LSC loop, not including boost (FM1) and integrator (FM2)
lsc_filters = [6,8,10]#added FM5, CLP30
# the time scale of the PZT ramp
pzt_ramp_time = 10
# step size of pzt sweep
pzt_step_size = -10
# value to reset to after leaving the PZT range
pzt_reset_value = -10
# acceptable range of H1:OMC-PZT2_OFFSET
pzt_range = [-50, 50]
# size of jump to put into PZT when locked on the wrong mode
pzt_jump_size = 1
# the ASC master gain when in QPD alignment
use_dither_asc = 0
asc_master_gain_qpd = 0.02
# the ASC master gain when in dither alignment
asc_master_gain_dither = 0.05
# this is the minimum power required on the QPD in order to enable alignment
qpd_power_threshold = 0.002 #Should be 0.005, lowering to 0.0025 JCD 6 June 2024
# time scale to for offloading dither outputs to TT
dither_offload_time = 6
# desired camera exposure setting
camera_exposure = 1000
# OMC trans Camera channel name prefix
camera_channel = 'VID-CAM20'
# Path to camera images
camera_images_directory = '/ligo/data/camera/'
# Prefix for camera images
camera_prefix = 'OMC_TRANS'
#camera_prefix = 'script_camera_images/omc_lock'

# DARM offset threshold 
darm_offset_LOLO = 1e-6 # lower threshold
darm_offset_HIHI = 1e-4 # upper threshold


# OMC scan settings
scan_tramp = 60 	# increased from 60 20230215, scan time scale [sec]
scan_vstart = -40 # was-20, changed by EMC 20230215, changed again 20230105 changed 20220702 glm # initial voltage for scan in [V], changed to make sure we lock on high voltage resonance Feb 18 2019 SED, changed again 20220401
scan_vramp = 70		# increased from 70 20230215, voltage to ramp in [V] # Made larger by 10 so we don't have to keep changing the vstart - 1Apr2022 TJ

# OMC peak finding parameters
num_smalldataset = 7 # number of the segments that the whole scan data is divided into

