from guardian import GuardState, NodeManager, GuardStateDecorator

import time
import gpstime
import numpy as np
from scipy import signal
from matplotlib import mlab
import cmath
import cdsutils
import omcparams
import ISC_library
import timeout_utils

nodes = NodeManager(['SUS_OMC'])

##################################################
# Functions used only in OMC
##################################################

class assert_light_on_qpds(GuardStateDecorator):
    """Decorator that jumps to DOWN state if no light on qpds."""
    def pre_exec(self):
        sumA = 0
        sumB = 0
        for i in range(11):
            sumA += ezca['ASC-OMC_A_SUM_OUT16']
            sumB += ezca['ASC-OMC_B_SUM_OUT16']
            time.sleep(0.1)

        qpda_sum = sumA/11.0
        qpdb_sum = sumB/11.0

        if not (qpda_sum > omcparams.qpd_power_threshold) and (qpdb_sum > omcparams.qpd_power_threshold):
            log('QPD LIGHT GONE! returning to down...')
            return 'DOWN'
        if nodes['SUS_OMC'] != 'ALIGNED':
            notify('OMC suspension is not aligned')
            return 'FAULT'

            
def whitening_change_helper(comp_filt_bank, analog_bypass_sw, action):    
    """
    comp_filt_bank : ezca object 
        filter bank in which DCPD whitening
        is compensated 
    analog_bypass_sw : string
        channelname of DCPD whitening relay switch
    action : string
        'add' or 'remove'
    """
    
    if action == 'add':
        if ezca[analog_bypass_sw] != 0:
            ezca[analog_bypass_sw] = 0    # Turn analog bypass OFF, i.e. whitening ON
            comp_filt_bank.switch('FM2', 'ON') # Turn digital compensation ON, to compensate whitening ON
    if action == 'remove':
        if ezca[analog_bypass_sw] != 1:
            ezca[analog_bypass_sw] = 1    # Turn analog bypass ON, i.e. whitening OFF
            comp_filt_bank.switch('FM2', 'OFF') # Turn digital compensation OFF, nothing to compensate


##################################################
# STATES: 
##################################################
nominal = 'READY_FOR_HANDOFF'

class INIT(GuardState):
    index = 0
    request = True

    def main(self):
        nodes.set_managed()

    def run(self):
        if nodes['SUS_OMC'] != 'ALIGNED':
            notify('OMC suspension is not aligned')
            return 'FAULT'
        return 'DOWN'


class FAULT(GuardState):
    request = False
    redirect = False

    def main(self):
        # LSC switches
        ezca['OMC-LSC_SERVO_GAIN'] = 0
        ezca.switch('OMC-LSC_SERVO','FM1','FM2','FM3','OFFSET','OFF')
        ezca.switch('OMC-LSC_SERVO','FM6','FM8','FM10','ON')

        # ASC switches
        ezca['OMC-ASC_MASTERGAIN'] = 0
        ezca['OMC-ASC_QDSLIDER'] = 1
        for filt in ['POS_X', 'POS_Y', 'ANG_X', 'ANG_Y']:
            ezca.switch('OMC-ASC_' + filt, 'FM2', 'OFF')

    def run(self):
        if nodes['SUS_OMC'] != 'ALIGNED':
            notify('OMC suspension is not aligned')
            return True
        return 'DOWN'


class DOWN(GuardState):
    index = 110
    request = True
    goto = True

    def main(self):
        # Turn it off
        ezca['OMC-LSC_SERVO_GAIN'] = 0
        ezca['OMC-ASC_MASTERGAIN'] = 0

        # LSC switches
        servofilt = ezca.get_LIGOFilter('OMC-LSC_SERVO')
        servofilt.only_on('INPUT', 'LIMIT', 'OUTPUT', 'DECIMATION', *omcparams.lsc_filters)
        ezca.switch('OMC-DCPD_NORM_FILT','INPUT','ON')
        ezca['OMC-LSC_LOCK_TRIGGER_THRESHOLD'] = omcparams.lsc_trigger_threshold

        # ASC switches
        ezca['OMC-ASC_DITHER_MASTER'] = 0

        # DCPD gains (in case we were interrupted in whitening switch
        ezca['OMC-DCPD_A_GAIN'] = 1
        ezca['OMC-DCPD_B_GAIN'] = 1


    def run(self):
        if nodes['SUS_OMC'] == 'ALIGNED':
            return True
        else:
            notify('OMC suspension is not aligned')


class OFF_RESONANCE(GuardState):
    index = 3
    request = True

    def run(self):
        if nodes['SUS_OMC'] == 'ALIGNED' and ISC_library.PSL_ready():
            if ezca['OMC-DCPD_SUM_OUT16'] > 0.05 * ezca['PSL-POWER_SCALE_OFFSET']:
                if ezca['OMC-PZT2_OFFSET'] > 50:
                    ezca['OMC-PZT2_OFFSET'] = -50
                else:
                    ezca['OMC-PZT2_OFFSET'] += 0.25           
            return True
        else:
            notify('OMC suspension is not aligned or PSL not ready')


class WAIT_FOR_AS_LIGHT(GuardState):
    index = 5
    request = False
    
    def main(self):
        self.timer['wait'] = 0

    def run(self):
        if self.timer['wait']:
            if ezca['SYS-MOTION_C_FASTSHUTTER_A_STATE'] == 1:
                notify('Toast is ready!')
                ezca['ISI-HAM6_WD_RSET'] = 1
                time.sleep(0.5)
                ezca['SYS-MOTION_C_FASTSHUTTER_A_UNBLOCK'] = 1 # open fast shutter

            if nodes['SUS_OMC'] == 'ALIGNED':
                sumA = 0
                sumB = 0
                #take an average of the OMC QPD sums
                for i in range(11):
                    sumA += ezca['ASC-OMC_A_SUM_OUT16']
                    sumB += ezca['ASC-OMC_B_SUM_OUT16']
                    time.sleep(0.1)
                qpda_sum = sumA/11.0
                qpdb_sum = sumB/11.0
                print(qpda_sum)
                print(qpdb_sum)
                if (qpda_sum > omcparams.qpd_power_threshold) and (qpdb_sum > omcparams.qpd_power_threshold):
                    return True
                else:
                    notify('Not enough light on OMC QPDs. Waiting...')
                    self.timer['wait'] = 2
            else:
                notify('OMC suspension is not aligned')
        else:
            return False


class ASC_QPD_ON(GuardState):
    index = 120
    request = True

    @assert_light_on_qpds
    def main(self):
        #turn on QPD loops
        ezca['OMC-ASC_MASTERGAIN'] = 0
        ezca['OMC-ASC_QDSLIDER'] = 1.0 

        ezca['OMC-ASC_POS_X_GAIN'] = 0.3
        ezca['OMC-ASC_POS_Y_GAIN'] = 0.4
        ezca['OMC-ASC_ANG_X_GAIN'] = 0.175
        ezca['OMC-ASC_ANG_Y_GAIN'] = -0.35

        # Turn on the gain gently, the compensation filters for the OMC SUS have some transient response
        #cdsutils.step(ezca, 'OMC-ASC_MASTERGAIN','0.01,2',0.2)
        #SED change April 11th
        ezca['OMC-ASC_MASTERGAIN'] = omcparams.asc_master_gain_qpd

        for filt in ['POS_X', 'POS_Y', 'ANG_X', 'ANG_Y']:
            ezca.switch('OMC-ASC_' + filt, 'FM2', 'ON')


class PREP_OMC_SCAN(GuardState):
    index = 140
    request = False

    @assert_light_on_qpds
    def main(self):
        # turn off all the DCPD whitening switches as a prep for the later states
        #for ii in ['1','2','3']:
        #    for PD in ['A','B']:
        #        ezca['OMC-DCPD_' + PD + '_WHITEN_SET_' + ii] = 0
        #        ezca.switch('OMC-DCPD_' + PD,'FM' + ii,'OFF')
        
        # initialize the PZT settings 
        ezca['OMC-PZT2_TRAMP'] = 0
        ezca['OMC-PZT2_OFFSET'] = omcparams.scan_vstart 
        self.timer['sleep'] = 0

        # set OMC_DC filter module
        ezca.get_LIGOFilter('LSC-OMC_DC').only_on('INPUT','OFFSET', 'OUTPUT', 'DECIMATION')

    @assert_light_on_qpds
    def run(self):
        if self.timer['sleep']:
            # check if DARM offset is resonable 
            darm_offset = ezca['LSC-DARM1_OFFSET']
            if abs(darm_offset)< omcparams.darm_offset_LOLO or abs(darm_offset)>omcparams.darm_offset_HIHI:
                notify('DARM offset is %f Check LSC-DARM1_OFFSET.'%(darm_offset))
                self.timer['sleep'] = 1 # sleep until somebody activates DARM offset
            else:
                return True    


class FIND_CARRIER(GuardState):
    """Sweep the OMC cavity with PZT2 to find the carrier and two 45MHz
    sidebands. Try going back to the carrier position accounting for hysteresis
    or other effects, and scan around for an acceptable resonance.
    """
    index = 150
    request = True
    @assert_light_on_qpds
    def main(self):

        self.complete = False

        # Move these to omcparams later
        min_pzt_sb_spacing = 3.5
        max_pzt_sb_spacing = 10.5
        

        ezca['OMC-PZT2_TRAMP'] = omcparams.scan_tramp
        gps_start = int(gpstime.gpsnow())
        log('Scan starting at {}'.format(gps_start))

        # sweep PZT
        ezca['OMC-PZT2_OFFSET'] = omcparams.scan_vramp + omcparams.scan_vstart
        log('Sweeping OMC to find carrier and 45MHz sidebands...')

        # sleep until the scan finishes
        time.sleep(omcparams.scan_tramp)
        log('Sweep finished')

        # Go to the low end of the voltage ramp so that we don't have to worry about PZT hysteresis
        ezca['OMC-PZT2_TRAMP'] = 0.1
        time.sleep(0.1)
        ezca['OMC-PZT2_OFFSET'] = omcparams.scan_vstart
        time.sleep(0.3)

        # TJ - 2022-03-16
        # Due to an NDS bug, we cant reliably get data in the past when the timeframe
        # spans data that is on both memory and on disk. A temp workaround for this is
        # to ask for more data than we need (~300s works) and then only use what we need.
        # If this isnt working well enough to hold us over until the bug is fixed,
        # we may have to get more creative or wait some time to make sure the data is on disk.
        log('Fetching data...')

        additional_getdata_time = 240
        def get_omc_data(additional_getdata_time=240):
            channels = ['H1:OMC-PZT2_MON_DC_OUT_DQ', 'H1:OMC-DCPD_SUM_OUT_DQ']
            duration = omcparams.scan_tramp + additional_getdata_time
            start = gps_start - additional_getdata_time
            data_buffers = timeout_utils.call_with_timeout(cdsutils.getdata, channels, duration, start=start)
            return data_buffers

        omc_data = get_omc_data()
        if None in omc_data:
            log('Data was not found, trying again to fetch data...')
            omc_data = get_omc_data()
            if None in omc_data:
                log('Failed to get data again. Waiting 30s then trying one last time.')
                time.sleep(30)
                omc_data = get_omc_data()
                if None in omc_data:
                    log('Failed on last attempt, possible NDS issue.')
                    raise NodeError('Unable to get past data, check NDS.')
        log('data fetched')

        # Channel rates:
        #    H1:OMC-PZT2_MON_DC_OUT_DQ - 512
        #    H1:OMC-DCPD_SUM_OUT_DQ - 16384
        pzt_sr = int(omc_data[0].sample_rate)
        pzt = omc_data[0].data[additional_getdata_time*pzt_sr:]

        dcpd_sr = int(omc_data[1].sample_rate)
        dcpd_sum_fast_data = omc_data[1].data[additional_getdata_time*dcpd_sr:]
        dcpd_sum = dcpd_sum_fast_data

        def sum_ind_to_pzt_ind(index):
            """Convert the index that the peak finder gives
            into an equivalent one for the pzt.
            """
            return int(index * pzt_sr / dcpd_sr)

        # Use the scipy signal find_peaks to get the three peaks (45Mhz, carrier, 45Mhz)
        # Prominence seems to work best. Prominence is "the minimum height necessary to
        # descend to get from the summit to any higher terrain"
        peaks, peak_details = signal.find_peaks(dcpd_sum, height=5, prominence=5)  #lower hieght to 6 if DARM offset is reduced to 6e-5, prominice to 5 from 8
        log('Find peaks output (peaks, peak_output): {}'.format(peaks, peak_details))

        log('Peaks found at pzt2_mon positions: {}'.format([pzt[sum_ind_to_pzt_ind(peak)] for peak in peaks]))

        # Verify the number of peaks makes sense, then find the pzt position that matches with carrier
        if len(peaks) >= 3:
            self.trident_pzt_v = []
            trident_groups = []
            trident_group_heights = []
            # Find the indexes of the three peaks with valid separation
            for i in range(len(peaks)):
                if i == 0 or i == 1:
                    continue
                else:
                    right_peak_pzt = pzt[sum_ind_to_pzt_ind(peaks[i])]
                    middle_peak_pzt = pzt[sum_ind_to_pzt_ind(peaks[i-1])]
                    left_peak_pzt = pzt[sum_ind_to_pzt_ind(peaks[i-2])]
                    if (min_pzt_sb_spacing < abs(right_peak_pzt - middle_peak_pzt) < max_pzt_sb_spacing) \
                            and (min_pzt_sb_spacing < abs(middle_peak_pzt - left_peak_pzt) < max_pzt_sb_spacing):
                        self.trident_pzt_v.extend([right_peak_pzt, middle_peak_pzt, left_peak_pzt])
                        trident_groups.append([right_peak_pzt, middle_peak_pzt, left_peak_pzt])
                        trident_group_heights.append(peak_details['peak_heights'][i - 2:i + 1])
            # Added this conditional to handle when we get two tridents in our scan --TJS May 19 2023
            if len(trident_groups) > 1:
                log(f'Found two trident groups: {trident_groups}')
                tri_avgs = []
                for group in trident_groups:
                    tri_avgs.append(sum(group)/len(group))
                best_group = tri_avgs.index(max(tri_avgs))
                log(f'Group {best_group} had highest avg, choosing this')
                self.trident_pzt_v = trident_groups[best_group]
            elif len(self.trident_pzt_v) > 3:
                log('Too many peaks to sort through, resetting and trying again')
                return 'DOWN'
            elif len(self.trident_pzt_v) < 3:
                log('Found peaks didnt have valid separation, resetting and trying again')
                return 'DOWN'
            else:
                # WE FOUND THEM!
                log('SB, Carrier, SB pzt2_mon positions: {}'.format(self.trident_pzt_v))
        else:
            log('Didn\'t find enough peaks, resetting and trying again')
            return 'DOWN'

    @assert_light_on_qpds
    def run(self):
        if self.complete:
            return True
        nom_carrier_v = self.trident_pzt_v[1]
        # Dont search too far and accidentally find a sideband
        v_range = 3.5
        v_step = 0.1
        # Scanning with the pz seems to have some type of delay/hysteresis, so this
        # fudge factor slightly accounts for it. It is really unfortunate
        # to have though because its dependent on the scan rate and direction
        v_fudge = -1.5

        # Try to go straight to where it should be, then move if below some threshold
        # H1:OMC-PZT2_MON_DC_OUT seems to be 50 greater than H1:OMC-PZT2_OFFSET
        ezca['OMC-PZT2_TRAMP'] = 0.5
        nom_carrier_offset = nom_carrier_v - 50 + v_fudge
        log('Moving to carrier offset = {}'.format(nom_carrier_offset))
        ezca['OMC-PZT2_OFFSET'] = nom_carrier_offset
        time.sleep(2)

        acceptable_height = 2
        if ezca['OMC-DCPD_SUM_OUTPUT'] > acceptable_height:
            log('Resonant, DCPD sum = {}'.format(ezca['OMC-DCPD_SUM_OUTPUT']))
            return True
        else:
            # Counter is for scan one direction, count, then scan other direction
            counter = 0
            # FIXME: change this from a while loop to just using the run methods loop
            while ezca['OMC-DCPD_SUM_OUTPUT'] < acceptable_height:
                log('DCPD sum = {}'.format(ezca['OMC-DCPD_SUM_OUTPUT']))
                # Already scanned both directions
                if counter > 1:
                    log('Couldnt find the carrier light')
                    return False
                # Initial move
                elif counter == 0:
                    if ezca['OMC-PZT2_OFFSET'] < nom_carrier_offset + v_range:
                        ezca['OMC-PZT2_OFFSET'] += v_step
                        time.sleep(1)
                    else:
                        log('Reached (+) range, moving other direction')
                        ezca['OMC-PZT2_OFFSET'] = nom_carrier_offset
                        counter += 1
                        time.sleep(2)
                # Try moving other way
                elif counter == 1:
                    if ezca['OMC-PZT2_OFFSET'] > nom_carrier_offset - v_range:
                        ezca['OMC-PZT2_OFFSET'] -= v_step
                        time.sleep(1)
                    else:
                        log('Scanned full (-) range, no carrier. Returning DOWN to try again.')
                        return 'DOWN'
            log('Found carrier resonance!')
            self.complete = True
            return True


# Lock the OMC on the carrier TEM00 resonance.  
class OMC_LSC_ON(GuardState):
    index = 200
    request = False

    @assert_light_on_qpds
    def main(self):
        # check that everything is starting from the OFF position
        ezca['OMC-LSC_SERVO_TRAMP'] = 0
        ezca.switch('OMC-LSC_PD_IN','FM1','ON')  # lowpass filter before length dither demodulation
        ezca.switch('OMC-LSC_SERVO','OFFSET','OFF')
        ezca.switch('OMC-DCPD_NORM_FILT','INPUT','ON')
        ezca.switch('OMC-DCPD_NORM_FILT','OFFSET','OFF')
        ezca.switch('OMC-LSC_SERVO','FM1','FM2','FM3','INPUT','OFF')
        ezca['OMC-LSC_OSC_FREQ'] = omcparams.lsc_dither_freq
        ezca['OMC-LSC_LOCK_TRIGGER_THRESHOLD'] = omcparams.lsc_trigger_threshold

        # Turn on one stage of DCPD whitening
        #ezca['OMC-DCPD_A_WHITEN_SET_1'] = 1
        #ezca.switch('OMC-DCPD_A','FM1','ON')
        #ezca['OMC-DCPD_B_WHITEN_SET_1'] = 1
        #ezca.switch('OMC-DCPD_B','FM1','ON')

        # Turn on DCPD analog low pass
        #ezca['OMC-DCPD_A_WHITEN_SET_2'] = 1
        #ezca.switch('OMC-DCPD_A','FM2','ON')
        #ezca['OMC-DCPD_B_WHITEN_SET_2'] = 1
        #ezca.switch('OMC-DCPD_B','FM2','ON')

        self.timer['wait'] = 0.5
        self.counter = 1
        
    @assert_light_on_qpds   
    def run(self):

        if ezca['OMC-LSC_LOCK_TRIGGER_LOCKMON']:
            if self.counter == 1 and self.timer['wait']:
                log('Resonance found!  Engaging OMC LSC servo.')
                ezca['OMC-LSC_SERVO_GAIN'] = omcparams.lsc_gain
                ezca.switch('OMC-LSC_SERVO','FM3','ON')
                self.timer['wait'] = 0.5
                self.counter += 1

            if self.counter == 2 and self.timer['wait']:
                ezca.switch('OMC-LSC_SERVO','INPUT','ON')

                # offload LSC control to PZT
                ezca['OMC-PZT2_TRAMP'] = 0.1
                ezca['OMC-PZT2_OFFSET'] += 0.001
                self.timer['wait'] = 1
                self.counter += 1

            if self.counter == 3 and self.timer['wait']:
                ctrl = timeout_utils.call_with_timeout(cdsutils.avg, 3, 'OMC-LSC_SERVO_OUT16')
                ezca['OMC-PZT2_OFFSET'] += round(ctrl,4)
                self.counter += 1

            if self.counter == 4 and self.timer['wait']:
                ctrl = timeout_utils.call_with_timeout(cdsutils.avg, 1, 'OMC-LSC_SERVO_OUT16')
                ezca['OMC-PZT2_OFFSET'] += round(ctrl,4)
                
                ezca.switch('OMC-LSC_SERVO','FM1','FM2','ON')
                self.counter += 1

        else:
            notify('We missed the mode! Please find carrier resonance by hand.')
            self.counter = 1 # reset counter to value from main
            return 'DOWN'

        if self.counter == 5:
            return True


# Requestable state with OMC resonating before handoff
class OMC_LOCKED(GuardState):
    index = 250
    request = True

    @assert_light_on_qpds  
    @ISC_library.assert_dof_locked_gen(['OMC'])
    def run(self):
        if ezca['OMC-DCPD_SUM_OUTPUT'] > 5:  #if DARM offset is lowered to 6e-5, set this to 6 instead of 10
            return True
        else:
            notify('wrong mode?')


# Switch to dither alignment
# When in doubt, swing the DITHER/QPD slider back to the QPDs and reduce the master gain to 0.5 or lower
class DITHER_ON(GuardState):
    index = 350
    request = True

    @assert_light_on_qpds
    @ISC_library.assert_dof_locked_gen(['OMC'])
    def main (self):
        # turn off ASC dither (default) 20Mar2023
        # for noise removal cf LHO ALOG 67936
        ezca['OMC-ASC_DITHER_MASTER'] = 0
        time.sleep(1)
        if omcparams.use_dither_asc:
            # turn on ASC dither when use_dither_asc is nonzero
            ezca['OMC-ASC_DITHER_MASTER'] = 1
            time.sleep(1)

            # switch to dither from QPDs, gently
            #taking the master gain to zero in 4 steps, separated by 0.3 seconds
            #SED comment, I think it would be fine to just slam the gains around, that is how I have done it manually without any apparent problems
            gain_step = -1*ezca['OMC-ASC_MASTERGAIN'] / 4
            cdsutils.step(ezca, 'OMC-ASC_MASTERGAIN', str(gain_step) + ',4',0.3)
            time.sleep(1.2)
            ezca['OMC-ASC_QDSLIDER'] = 0
            cdsutils.step(ezca, 'OMC-ASC_MASTERGAIN','0.01,2',0.5)
            ezca['OMC-ASC_MASTERGAIN'] = omcparams.asc_master_gain_dither
        else:
            return True


# Check that offsets and scale factor for LSC-OMC_DC are set for the handoff to DC readout
#
# Three things need to happen before the DARM handoff from ASQ --> DC readout can occur:
#       1) The power normalization has to be accounted for in the LSC input matrix.  
#          This is handled by the ISC_LOCK guardian.
#
#       2) A gain setting must be adjusted to match LSC-OMC_DC to the input to DARM; 
#          this is a measure of the sensitivity of DCPD_SUM to DARM length changes and 
#          varies as a function of the DARM offset.  This is done by changing 
#          H1:OMC-READOUT_SCALE_OFFSET so that the ratio of 
#          LSC-DARM_IN1 / LSC-OMC_DC_OUT is around 1 (+/- 5 percent) in a frequency 
#          band where the coherence is good, usually between 1-10Hz.
#          See the following DTT template: /ligo/home/daniel.hoak/dtt/OMC/OMC_DARM_handoff.xml.
#          Also the relative phase should be zero or you will not go to DC readout today.
#
#       3) An offset must be adjusted so that LSC-OMC_DC has the same overall value as the input to DARM.  
#          This is done by tuning LSC-OMC_DC_OFFSET
#          (which comes after the gain that was adjusted in #2) so that the average value of 
#          LSC-OMC_DC_OUT matches LSC-DARM_IN1.
#
class TUNE_OFFSETS(GuardState):
    index = 400
    request = True

    @assert_light_on_qpds  
    @ISC_library.assert_dof_locked_gen(['OMC'])
    def main(self):
        self.errorFlag = False
        # Set up the OMC-READOUT path.  Turn off the SIMPLE scaling path, enable the ERR path, make sure the X0 offset gain is 1.
        # Set the LSC-OMC_DC_OFFSET to a reasonable value, this will be changed later
        ezca['OMC-READOUT_SIMPLE_GAIN'] = 0
        ezca['OMC-READOUT_ERR_GAIN'] = 1
        ezca['OMC-READOUT_X0OFFSET_GAIN'] = 1
        ezca.get_LIGOFilter('LSC-OMC_DC').only_on('INPUT','OFFSET', 'OUTPUT', 'DECIMATION')
        
        self.counter = 1 # Initialize
        self.try_counter = 1 # Initialize

    def run(self):

        # No matter what, the OMC should be locked longitudinally
        omc_locked = ezca['OMC-LSC_LOCK_TRIGGER_LOCKMON']
        if not omc_locked:
            return 'DOWN'

        if self.counter == 1:
            if np.isnan(ezca['OMC-READOUT_ERR_GAIN']):
                ezca['OMC-READOUT_ERR_GAIN'] = 1
            GPS_start = ezca['FEC-8_TIME_DIAG']
            log(GPS_start)

            log('Fetching DARM-OMC TF Data')
            Tdata = 25
            data_dur = 300
            time.sleep(Tdata)
           
            # Read in DARM_IN1 and OMC-DCPD_SUM so we can match gains
            # We want to match the gain using OMC-READOUT_ERR_GAIN, so turn the crank on 
            #   OMC-DCPD_SUM to work through the READOUT_ERR path calculations

            # See note in FIND_CARRIER state. TLDR; inconsistent data grab for ~20-60sec of past data
            # Requesting more data (~300s) and then only using what you need is a workaround
            # Previous call: cdsutils.getdata(['H1:LSC-DARM_IN1_DQ','H1:OMC-DCPD_SUM_OUT_DQ'], Tdata, GPS_start-8)
            def get_darm_omc_data():
                return timeout_utils.call_with_timeout(cdsutils.getdata,
                                                       ['H1:LSC-DARM_IN1_DQ', 'H1:OMC-DCPD_SUM_OUT_DQ'],
                                                       data_dur,
                                                       start=(GPS_start-285))
            z = get_darm_omc_data()
            # Check that we actually get the data
            if None in z:
                log('Data was not retrieved, trying again')
                self.try_counter += 1
                return False
            else:
                log('data retrieved')
                
            fs = int(z[0].sample_rate) # Hz
            last_time_chunk = data_dur - Tdata # s
            DARM_IN = z[0].data[last_time_chunk * fs:]
            OMC_IN = z[1].data[last_time_chunk * fs:]

            # Set the PREF offset so that the DCPD Sum is properly normalized
            # P_AS * Pref / P0 = (x0**2 / xf**2)
            # --> Pref = 
            setpt = (ezca['OMC-READOUT_X0_OFFSET']**2) / (ezca['OMC-READOUT_XF_OFFSET']**2)
            ezca['OMC-READOUT_PREF_OFFSET'] = (setpt + ezca['OMC-READOUT_CD_OUTPUT']) * ezca['OMC-READOUT_TRAVG'] / np.median(OMC_IN)

            # calculate the output of the OMC-READOUT path so we can match the gain to DARM_IN1
            omc_out_calib = ezca['OMC-READOUT_PREF_OUTPUT'] / ezca['OMC-READOUT_TRAVG'] /2 * ezca['OMC-READOUT_XF_OUTPUT']**2 / ezca['OMC-READOUT_X0SAT'] * ezca['OMC-READOUT_ERR_GAIN']
            OMC_OUT = OMC_IN * omc_out_calib

            log('OMC Readout ERR GAIN = %s'%ezca['OMC-READOUT_ERR_GAIN'])  # debug 21Mar2022

            ### compute transfer function and coherence in the 1-15Hz band
            # length of each FFT, 3 second strides
            stride = 3.0
            nfft = int(stride * fs) 

            # calculate PSDs and CSD
            Pxx,fr = mlab.psd(DARM_IN, NFFT=nfft, Fs=fs, noverlap=nfft/2)
            Pyy,fr = mlab.psd(OMC_OUT, NFFT=nfft, Fs=fs, noverlap=nfft/2)
            Pxy,fr = mlab.csd(DARM_IN, OMC_OUT, NFFT=nfft, Fs=fs, noverlap=nfft/2)

            # find the correct bin index
            idx1 = np.argmin(abs(fr-1.0))
            idx2 = np.argmin(abs(fr-15.0))

            # compute TF and coherence at the bin
            TFxy = Pxy[idx1:idx2] / Pxx[idx1:idx2]
            COHxy = abs(Pxy[idx1:idx2])**2 / (Pxx[idx1:idx2] * Pyy[idx1:idx2])
            log('Values of COHxy: %s'%COHxy)

            # only use data with good coherence
            coherence_threshold = 0.6
            good_idx = np.where(COHxy > coherence_threshold)[0]
            log('number of good coherence indices: %s'%np.size(good_idx))  # debug 21Mar2022

            if np.size(good_idx) != 0:
                self.counter += 1
            else:
                log(f'No measured coherence between DARM_IN and OMC_OUT above coherence threshold = {coherence_threshold}')
                log(f'Trying again to measure TF')

        if self.counter == 2:

            # Get median transfer function value in the good coherence realm
            good_tf = TFxy[good_idx]
            good_tf_real = np.real(good_tf)
            good_tf_imag = np.imag(good_tf)
            median_good_tf_real = np.median(good_tf_real)
            median_good_tf_imag = np.median(good_tf_imag)
            median_good_tf = median_good_tf_real + 1j * median_good_tf_imag

            TFmag = np.abs(median_good_tf)
            TFphase = np.angle(median_good_tf, deg=True)
            log('Complex TF parameters (magnitude and phase):')
            log(TFmag)
            log(TFphase)

            correction_sign = 1.0
            if np.median(abs(TFphase)) > 150:
                correction_sign = -1.0
            elif np.median(abs(TFphase)) < 30:
                correction_sign = 1.0
            else:
                log('TF phase is weird!!')
                self.errorFlag = True
            
            #scale_start = ezca['OMC-READOUT_SCALE_OFFSET']
            scale_start = ezca['OMC-READOUT_ERR_GAIN']
            log('Scale start:')

            log(scale_start)
            scale_correction = correction_sign * scale_start / TFmag
            log('Scale calculation:')
            log(scale_correction)

            ezca['OMC-READOUT_ERR_GAIN'] = scale_correction
            time.sleep(1)
            ezca['LSC-OMC_DC_OFFSET'] = -ezca['LSC-DARM1_OFFSET']  #if you take into account the power normalization, the gain from LSC-OMC_DC to DARM IN1 is 1
            time.sleep(ezca['LSC-OMC_DC_TRAMP']+1)

            self.counter += 1


        if self.counter == 3:

            if self.errorFlag:
                if self.try_counter > 2:
                    # If we've tried to re-get this TF data and it still doesn't look good, try relocking the OMC.
                    log('Failed too many times to get good TF data, so try relocking the OMC')
                    return 'DOWN'

                self.counter = 1 # Reset to 1, so that it will retry taking the TF data.
                self.try_counter += 1 # Make note of how many times we've tried to get this TF data, so that it will jump out and relock if needed.

                #log('Something is wrong! Transfer function is weird! Not proceeding! Try locking OMC again')
                log('Failed to get a good TF.  Trying again')

            else:
                return True


# Final state
def gen_READY_STATE(initial_wait=20):
    class READY_STATE(GuardState):
        request = True
        @assert_light_on_qpds
        @ISC_library.assert_dof_locked_gen(['OMC'])
        def main(self):
            self.timer['vio_check_pause'] = initial_wait # Make longer than 2 secs, just so OMC can rest and settle after

        @assert_light_on_qpds
        @ISC_library.assert_dof_locked_gen(['OMC'])
        def run(self):

            if self.timer['vio_check_pause']:
                # Check h1omc's adc0 channels 12 and 13 to see if OMC DCPD A or B inputs are saturating
                if (ezca['FEC-179_ADC_OVERFLOW_0_12'] > 0) or (ezca['FEC-179_ADC_OVERFLOW_0_13'] > 0):
                    self.timer['vio_check_pause'] = 20
                    notify('OMC DCPDs are saturating')
                    # I dont think we want this to return False if there was just a quick glitch
                else:
                    return True

            #notify('OMC is ready to go! :-D')
            return True
    return READY_STATE


def gen_WHITENING_CHANGE(action):
    class WHITENING_CHANGE(GuardState):
        request = False
        '''
        Change the whitening state of the DCPDs.
        This state is gentle enough to be used in full lock.
        '''

        def main(self):
            self.fail = False
            
            # LHO:67319 provides key to D2200215-style whitening chassis remote control bits. 
            # "Off" == 0 == whitening in ON, b/c bypass is OFF
            dcpdA_bypass_chan = 'OMC-DCPD_A_GAINSET'
            dcpdB_bypass_chan = 'OMC-DCPD_B_GAINSET'
            
            # Get DCPD filters and gains, and set ramp times
            dcpdA_filterbank_obj = ezca.get_LIGOFilter('OMC-DCPD_A0') # w/ new 524 kHz system, Whitening Compensation lives in 54 kHz A0/B0 banks
            dcpdB_filterbank_obj = ezca.get_LIGOFilter('OMC-DCPD_B0')
            pi_filterbank_obj = ezca.get_LIGOFilter('OMC-PI_DOWNCONV_SIG')
            
            dcpdA_filterbank_obj.TRAMP.put(3)
            dcpdB_filterbank_obj.TRAMP.put(3)
            dcpdA_gain = dcpdA_filterbank_obj.GAIN.get()
            dcpdB_gain = dcpdB_filterbank_obj.GAIN.get()
            
            # Validate whitening change request    
            if action == 'add' and not ezca['OMC-DCPD_A_GAINSET']:
                notify('Whitening is ON already!') # code assumes if DCPDA (OMCDCPD C RELAY) is ON, then DCPDB whitening is ON.
                self.fail = True

            elif action == 'remove' and ezca['OMC-DCPD_A_GAINSET']:
                notify('Whitening is already OFF!')    
                self.fail = True
            else:
                log(action)
                log(ezca['OMC-DCPD_C_RELAYSET'])
                
            # Begin to take action    
            if self.fail:
                pass
            else:
                if ezca['GRD-ISC_LOCK_STATE_N'] > 430: # If we are past DC Readout Transition, state 500
                    log('Ramping from 2 DCPDs to 1 DCPD since we are using OMC')
                    
                    # Switch DARM over entirely to A, then change whitening on B
                    dcpdA_filterbank_obj.GAIN.put(2*dcpdA_gain)
                    dcpdB_filterbank_obj.GAIN.put(0)
                    time.sleep(3)
                    
                    while dcpdA_filterbank_obj.is_gain_ramping() or dcpdB_filterbank_obj.is_gain_ramping():
                        time.sleep(1)
                        
                    log('Ramped to entirely to DCPDA, switching whitening on DCPDB')    
                    whitening_change_helper(dcpdB_filterbank_obj, dcpdB_bypass_chan, action)
                    time.sleep(1) # give ample time for analog switch to change
                    
                    # Switch DARM over entirely to B, then change whitening on A
                    dcpdA_filterbank_obj.GAIN.put(0)
                    dcpdB_filterbank_obj.GAIN.put(2*dcpdB_gain)
                    time.sleep(3)
                    
                    while dcpdA_filterbank_obj.is_gain_ramping() or dcpdB_filterbank_obj.is_gain_ramping():
                        time.sleep(1)
                    
                    log('Ramped to entirely to DCPDB, switching whitening on DCPDA')   
                    whitening_change_helper(dcpdA_filterbank_obj, dcpdA_bypass_chan, action)
                    time.sleep(1) # give ample time for analog switch to change
                    
                else:  # If IFO is not yet using OMC, just swap the whitening, no gain ramping
                    log('OMC not in use by IFO, rapidly switching both DCPD''s whitening')
                    whitening_change_helper(dcpdA_filterbank_obj, dcpdA_bypass_chan, action)
                    whitening_change_helper(dcpdB_filterbank_obj, dcpdB_bypass_chan, action)

                # Switch filter banks back to equal control of A and B
                dcpdA_filterbank_obj.GAIN.put(dcpdA_gain)
                dcpdB_filterbank_obj.GAIN.put(dcpdB_gain)
                if action == 'add':
                    pi_filterbank_obj.switch('FM2', 'ON')
                else:
                    pi_filterbank_obj.switch('FM2', 'OFF')
                return True 

        def run(self):
            dcpdA_filterbank_obj = ezca.get_LIGOFilter('OMC-DCPD_A0') # w/ new 524 kHz system, Whitening Compensation lives in 54 kHz A0/B0 banks
            dcpdB_filterbank_obj = ezca.get_LIGOFilter('OMC-DCPD_B0')
        
            if dcpdA_filterbank_obj.GAIN.get() != dcpdB_filterbank_obj.GAIN.get():
                notify('DCPD gains not equal after whitening switch -- yikes!')
            elif self.fail:
                notify('It did not make sense to '+action+' whitening, so did nothing.')
                return True
            else:
                return True
            
    return WHITENING_CHANGE

# Assign state generators
SET_WHITENING = gen_WHITENING_CHANGE('remove') # Want to *not* use whitening while locking, JCD 27Feb2023
SET_WHITENING.index = 130
ADD_WHITENING = gen_WHITENING_CHANGE('add')
ADD_WHITENING.index = 475
REMOVE_WHITENING = gen_WHITENING_CHANGE('remove')
REMOVE_WHITENING.index = 450
READY_W_NO_WHITENING = gen_READY_STATE()
READY_W_NO_WHITENING.index = 425
READY_FOR_HANDOFF = gen_READY_STATE(initial_wait=2)
READY_FOR_HANDOFF.index = 500

##################################################
# EDGES 
##################################################

edges = [
    ('INIT',                'DOWN'),
    ('DOWN',                'DOWN'), #adding an edge from DOWN to DOWN, this way when we are in DOWN and ISC_LOCK requests down, the main will be run again. 
    ('DOWN',                'OFF_RESONANCE'),
    ('DOWN',                'WAIT_FOR_AS_LIGHT'),
    ('WAIT_FOR_AS_LIGHT',   'ASC_QPD_ON'),
    ('ASC_QPD_ON',          'SET_WHITENING'),
    ('SET_WHITENING',       'PREP_OMC_SCAN'),
    ('PREP_OMC_SCAN',       'FIND_CARRIER'),
    ('FIND_CARRIER',        'OMC_LSC_ON'),
    ('OMC_LSC_ON',          'OMC_LOCKED'),
    ('OMC_LOCKED',          'DITHER_ON'),
    ('DITHER_ON',           'TUNE_OFFSETS'),
    #('TUNE_OFFSETS',        'READY_FOR_HANDOFF'),
    #('READY_FOR_HANDOFF',   'TUNE_OFFSETS'),
    ('TUNE_OFFSETS',        'READY_W_NO_WHITENING'),
    ('READY_W_NO_WHITENING','ADD_WHITENING'),
    ('ADD_WHITENING',       'READY_FOR_HANDOFF'),
    ('READY_FOR_HANDOFF',   'REMOVE_WHITENING'),
    ('REMOVE_WHITENING',    'READY_W_NO_WHITENING'),
    #('READY_FOR_HANDOFF',   'ADD_LOW_PASS'),
    #('ADD_LOW_PASS',        'READY_FOR_HANDOFF'),
    #('REMOVE_LOW_PASS',     'READY_FOR_HANDOFF'),
    #('READY_FOR_HANDOFF',   'REMOVE_LOW_PASS'),    
    ]

